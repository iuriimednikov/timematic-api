from flask_restful import Resource, reqparse
from models.time import TimeEntryModel
from flask_jwt import jwt_required

class CreateTimeEntryResource(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('userId', type=str, required=True)
    parser.add_argument('projectId', type=str, required=False)
    parser.add_argument('description', type=str, required=True)
    parser.add_argument('startTime', type=str, required=True)
    parser.add_argument('endTime', type=str, required=True)
    parser.add_argument('createdDate', type=str, required=True)

    @jwt_required()
    def post(self):
        data = CreateTimeEntryResource.parser.parse_args()
        entry = TimeEntryModel(_id=None, 
                                user_id=data['userId'], 
                                project_id=data['projectId'], 
                                description=data['description'],
                                created_date=data['createdDate'],
                                start_time=data['startTime'],
                                end_time=data['endTime'])
        result = entry.create()
        if result:
            return result, 200
        else:
            return {'message': 'Something went wrong'}, 500

class TimeEntriesListResource(Resource):

    @jwt_required()
    def get(self, user_id):
        entries = TimeEntryModel.find_all_by_user_id(user_id)
        return {'data': entries}, 200

class TimeEntryResource(Resource):

    @jwt_required()
    def delete(self, id):
        entry = TimeEntryModel.find_one_by_id(id)
        if entry:
            result = entry.delete()
            if result:
                return {'message': 'success'}, 200
        return {'message': 'Something went wrong'}, 500
