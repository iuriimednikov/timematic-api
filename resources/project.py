from flask_restful import Resource, reqparse
from models.project import ProjectModel
from flask_jwt import jwt_required

class CreateProjectResource(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('userId', type=str, required=True)
    parser.add_argument('projectName', type=str, required=True)
    parser.add_argument('billable', type=bool, required=True)
    parser.add_argument('rate', required=False)
    parser.add_argument('currency', type=str, required=False)
    parser.add_argument('color', type=str, required=False)
    parser.add_argument('clientId', type=str, required=False)
    
    @jwt_required()
    def post(self):
        data = CreateProjectResource.parser.parse_args()
        project = ProjectModel(None, data['userId'], data['projectName'], data['billable'], data['rate'], data['currency'], data['color'], data['clientId'])
        result = project.create()
        if result:
            return result, 200
        return {'message': 'Something went wrong'}, 500

class ProjectsListResource(Resource):

    @jwt_required()
    def get(self, user_id):
        projects = ProjectModel.find_all_by_user_id(user_id)
        return {'data': projects}, 200

class ProjectResource(Resource):

    @jwt_required()
    def delete(self, project_id):
        project = ProjectModel.find_one_by_id(project_id)
        if project:
            result = project.delete()
            if result:
                return {'result': 'success'}, 200
        return {'result': 'error'}, 500