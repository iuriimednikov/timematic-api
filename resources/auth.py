from flask_restful import Resource, reqparse
from models.user import UserModel

class SignupResource(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('email', required=True, type=str)
    parser.add_argument('password', required=True, type=str)
    parser.add_argument('firstName', required=True, type=str)
    parser.add_argument('lastName', required=True, type=str)

    def post(self):
        data = SignupResource.parser.parse_args()
        user = UserModel.find_by_email(data['email'])
        if user:
            return {'message': 'User already exists'}, 400
        new_user = UserModel(email=data['email'], password=data['password'], salt=None, _id=None, first_name=data['firstName'], last_name=data['lastName'])
        result = new_user.create()
        if result:
            return result, 200
        return {'message': 'Something went wrong'}, 500

class Login:

    @classmethod
    def do_login(cls, username, password):
        user = UserModel.find_by_email(username)
        if user:
            result = user.authorize(password)
            return result
        return None

class Identity:

    @classmethod
    def authorize(cls, identity):
        user = UserModel.find_by_id(identity['identity'])
        return user