from flask_restful import Resource, reqparse
from models.client import ClientModel
from flask_jwt import jwt_required

class CreateClientResource(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('userId', type=str, required=True)
    parser.add_argument('clientName', type=str, required=True)

    @jwt_required()
    def post(self):
        data = CreateClientResource.parser.parse_args()
        client = ClientModel(None, data['userId'], data['clientName'])
        result = client.create()
        if result:
            return {'clientId': result}, 200
        else:
            return {'message': 'Something went wrong'}, 500
    
class ClientResource(Resource):

    @jwt_required()
    def delete(self, client_id):
        client = ClientModel.find_one_by_id(client_id)
        if client:
            result = client.delete()
            if result:
                return {'result': 'success'}, 200
        return {'result': 'error'}, 500

class ClientsListResource(Resource):

    @jwt_required()
    def get(self, user_id):
        clients = ClientModel.find_all_by_user_id(user_id)
        return {'data': clients}, 200