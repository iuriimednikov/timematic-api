import datetime

def find_duration (start_time, end_time):
    st = datetime.datetime.strptime(str(start_time), '%H:%M:%S')
    en = datetime.datetime.strptime(str(end_time), '%H:%M:%S')
    difference = en - st
    return str(difference)