from flask import Flask, jsonify
from flask_restful import Api
from flask_cors import CORS
from resources.client import ClientResource, ClientsListResource, CreateClientResource
from resources.project import CreateProjectResource, ProjectsListResource, ProjectResource
from resources.time import CreateTimeEntryResource, TimeEntriesListResource, TimeEntryResource
from resources.auth import SignupResource, Login, Identity
from flask_jwt import JWT

import datetime

app = Flask(__name__)
api = Api(app)
CORS(app, origins='*', allow_headers={'Authorization', 'Access-Control-Allow-Origin', 'content-type'})
app.config['SECRET_KEY'] = 'secret'
app.config['JWT_EXPIRATION_DELTA'] = datetime.timedelta(seconds=7200)
app.config['JWT_AUTH_URL_RULE'] = '/v1/login'
app.config['JWT_AUTH_USERNAME_KEY'] = 'email'

jwt = JWT(app, Login.do_login, Identity.authorize)

@jwt.auth_response_handler
def response_handler(access_token, identity):
    print(identity)
    print(access_token)
    return jsonify({
        'accessToken': access_token.decode('utf-8'), 
        'userId': identity.id, 
        'firstName': identity.first_name, 
        'lastName': identity.last_name
        })

api.add_resource(ClientsListResource, '/v1/clients/<string:user_id>')
api.add_resource(ClientResource, '/v1/client/<string:client_id>')
api.add_resource(CreateClientResource, '/v1/clients')
api.add_resource(ProjectsListResource, '/v1/projects/<string:user_id>')
api.add_resource(CreateProjectResource, '/v1/projects')
api.add_resource(ProjectResource, '/v1/project/<string:project_id>')
api.add_resource(CreateTimeEntryResource, '/v1/time')
api.add_resource(TimeEntriesListResource, '/v1/entries/<string:user_id>')
api.add_resource(TimeEntryResource, '/v1/entry/<string:id>')
api.add_resource(SignupResource, '/v1/signup')

if __name__ == '__main__':
    app.run(port=5000, debug=True)