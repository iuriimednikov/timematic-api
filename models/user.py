import bcrypt
from uuid import uuid4
from werkzeug.security import safe_str_cmp
from models.connection import get_db_connection

class UserModel:
    def __init__(self, _id, email, password, salt, first_name, last_name):
        self.id = _id
        self.email = email
        self.password = password
        self.salt = salt
        self.first_name = first_name
        self.last_name = last_name

    def create(self):    
        user_id = str(uuid4())
        salt_value = bcrypt.gensalt()
        password_hash = bcrypt.hashpw(self.password.encode('utf-8'), salt_value)

        connection = get_db_connection()
        cursor = connection.cursor()

        statement = 'INSERT INTO users (user_id, email, password, salt_value, first_name, last_name) VALUES (%s, %s, %s, %s, %s, %s);'
        args = (user_id, self.email, password_hash, salt_value, self.first_name, self.last_name)

        try:
            cursor.execute(statement, args)
            connection.commit()
            connection.close()
            return {'userId': user_id}
        except:
            connection.close()
            return None
    
    def authorize(self, password):
        hashed = bcrypt.hashpw(password.encode('utf-8'), self.salt.encode('utf-8'))
        hash_decoded = hashed.decode('utf-8')

        if safe_str_cmp(self.password, hash_decoded):
            return self
        return None

    @classmethod
    def find_by_email(cls, email):
        connection = get_db_connection()
        cursor = connection.cursor(dictionary=True)
        statement = 'SELECT user_id, email, password, salt_value, first_name, last_name FROM users WHERE email = %s'
        args = (email,)
        cursor.execute(statement, args)
        row = cursor.fetchone()
        if row:
            user = UserModel(
                _id=row['user_id'],
                email = row['email'],
                password = row['password'],
                salt = row['salt_value'],
                first_name = row['first_name'],
                last_name = row['last_name']
            )
            connection.close()
            return user
        else:
            connection.close()
            return None

    @classmethod
    def find_by_id(cls, user_id):
        connection = get_db_connection()
        cursor = connection.cursor(dictionary=True)
        statement = 'SELECT user_id, email, password, salt_value, first_name, last_name FROM users WHERE user_id = %s'
        args = (user_id,)
        cursor.execute(statement, args)
        row = cursor.fetchone()
        if row:
            user = UserModel(
                _id=row['user_id'],
                email = row['email'],
                password = row['password'],
                salt = row['salt_value'],
                first_name = row['first_name'],
                last_name = row['last_name']
            )
            connection.close()
            return user
        else:
            connection.close()
            return None