import uuid
from models.connection import get_db_connection

class ProjectModel:
    def __init__(self, _id, user_id, name, billable, rate, currency, color, client_id):
        self.id = _id
        self.user_id = user_id
        self.name = name
        self.billable = billable
        self.rate = rate
        self.currency = currency
        self.color = color
        self.client_id = client_id
    
    def create(self):
        connection = get_db_connection()
        cursor = connection.cursor()
        project_id = str(uuid.uuid4())
        statement = 'INSERT INTO projects (project_id, user_id, project_name, billable, rate, currency, color, client_id) VALUES (%s,%s,%s, %s, %s, %s, %s, %s);'
        try:
            args = (project_id, self.user_id, self.name, self.billable, self.rate, self.currency, self.color, self.client_id)
            cursor.execute(statement, args)
            connection.commit()
            cursor.close()
            connection.close()
            return {'projectId': project_id}
        except:
            cursor.close()
            connection.close()
            return None

    def delete(self):
        connection = get_db_connection()
        cursor = connection.cursor()
        statement = 'DELETE FROM projects WHERE project_id = %s;'
        try:
            cursor.execute(statement, (self.id,))
            connection.commit()
            cursor.close()
            connection.close()
            return True
        except:
            cursor.close()
            connection.close()
            return False

    @classmethod
    def find_all_by_user_id(cls, user_id):
        connection = get_db_connection()
        cursor = connection.cursor(dictionary=True)
        statement = 'SELECT project_id, project_name, billable, rate, currency, color, client_id, client_name FROM projects LEFT JOIN clients USING(client_id) WHERE projects.user_id = %s ORDER BY project_name ASC;'
        params = (user_id,)
        cursor.execute(statement, params)
        results = cursor.fetchall()
        projects = []
        for row in results:
            client = {'clientId': row['client_id'], 'clientName': row['client_name']}
            project = {
                'projectName': row['project_name'],
                'client': client,
                'userId': user_id,
                'projectId': row['project_id'],
                'color': row['color'],
                'billable': bool(row['billable']),
                'rate': str(row['rate']),
                'currency': row['currency']
            }
            projects.append(project)
        cursor.close()
        connection.close()
        return projects
        
    @classmethod
    def find_one_by_id(cls, _id):
        connection = get_db_connection()
        cursor = connection.cursor(dictionary=True)
        statement = 'SELECT user_id, project_name, billable, rate, currency, color, client_id FROM projects WHERE project_id = %s;'
        params = (_id,)
        cursor.execute(statement, params)
        row = cursor.fetchone()
        print(row)
        connection.close()
        if row:
            project = ProjectModel(
                _id,
                row['user_id'],
                row['project_name'],
                bool(row['billable']),
                row['rate'],
                row['currency'],
                row['color'],
                row['client_id']
            )
            return project
        return None