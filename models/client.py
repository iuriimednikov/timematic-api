import uuid
from models.connection import get_db_connection

class ClientModel():
    def __init__(self, _id, user_id, name):
        self.id = _id
        self.user_id = user_id
        self.name = name
    
    def create(self):
        connection = get_db_connection()
        cursor = connection.cursor()
        client_id = str(uuid.uuid4())
        statement = 'INSERT INTO clients (client_id, user_id, client_name) VALUES (%s,%s,%s);'
        try:
            cursor.execute(statement, (client_id, self.user_id, self.name))
            connection.commit()
            cursor.close()
            connection.close()
            return {'clientId': client_id}
        except:
            cursor.close()
            connection.close()
            return None

    def delete(self):
        connection = get_db_connection()
        cursor = connection.cursor()
        statement = 'DELETE FROM clients WHERE client_id = %s;'
        try:
            cursor.execute(statement, (self.id,))
            connection.commit()
            cursor.close()
            connection.close()
            return True
        except:
            # print(error)
            cursor.close()
            connection.close()
            return False
    
    @classmethod
    def find_all_by_user_id(cls, user_id):
        connection = get_db_connection()
        cursor = connection.cursor(dictionary=True)
        statement = 'SELECT client_id, client_name FROM clients WHERE user_id = %s ORDER BY client_name ASC;'
        params = (user_id,)
        cursor.execute(statement, params)
        results = cursor.fetchall()
        clients = []
        for row in results:
            client = {'clientId': row['client_id'], 'userId': user_id, 'clientName': row['client_name']}
            clients.append(client)
        cursor.close()
        connection.close()
        return clients
    
    @classmethod
    def find_one_by_id(cls, _id):
        connection = get_db_connection()
        cursor = connection.cursor(dictionary=True)
        statement = 'SELECT client_name, user_id FROM clients WHERE client_id = %s;'
        params = (_id,)
        cursor.execute(statement, params)
        row = cursor.fetchone()
        print(row)
        connection.close()
        if row:
            client = ClientModel(_id, row['user_id'], row['client_name'])
            return client
        return None