from models.connection import get_db_connection
import uuid
from util.helpers import find_duration

class TimeEntryModel:
    def __init__(self, _id, user_id, project_id, description, created_date, start_time, end_time):
        self.id = _id
        self.user_id = user_id
        self.project_id = project_id
        self.description = description
        self.created_date = created_date
        self.start_time = start_time
        self.end_time = end_time
    
    def create(self):
        connection = get_db_connection()
        cursor = connection.cursor()
        entry_id = str(uuid.uuid4())

        statement = 'INSERT INTO time_entries (entry_id, user_id, description, project_id, started_at, ended_at, created_date) VALUES (%s, %s, %s, %s, %s, %s, %s);'
        args = (entry_id, self.user_id, self.description, self.project_id, self.start_time, self.end_time, self.created_date)

        try:
            cursor.execute(statement, args)
            connection.commit()
            connection.close()
            return {'entryId': entry_id}
        except:
            connection.close()
            return {'message': 'Something went wrong'}


    def delete(self):
        connection = get_db_connection()
        cursor = connection.cursor()
        statement = 'DELETE FROM time_entries WHERE entry_id = %s;'
        args = (self.id,)
        try:
            cursor.execute(statement, args)
            connection.commit()
            connection.close()
            return True
        except:
            connection.close()
            return False


    @classmethod
    def find_all_by_user_id(cls, user_id):
        connection = get_db_connection()
        cursor = connection.cursor(dictionary=True)
        statement = 'SELECT entry_id, description, started_at, ended_at, created_date, project_name, color, billable, rate, currency FROM time_entries LEFT JOIN projects USING(project_id) WHERE time_entries.user_id = %s ORDER BY created_date DESC, started_at DESC'
        args = (user_id,)
        cursor.execute(statement, args)
        rows = cursor.fetchall()
        entries = []
        for row in rows:
            start_time = row['started_at']
            end_time = row['ended_at']
            duration = find_duration(start_time = start_time, end_time = end_time)
            project = {
                'projectName': row['project_name'],
                'color': row['color'],
                'billable':bool(row['billable']),
                'rate':str(row['rate']),
                'currency':row['currency']
            }
            entry = {
                'project': project,
                'userId': user_id,
                'entryId': row['entry_id'],
                'createdDate':str(row['created_date']),
                'description': row['description'],
                'duration': duration,
                'startTime':str(row['started_at']),
                'endTime':str(row['ended_at'])
            }
            entries.append(entry)
        connection.close()
        return entries

    @classmethod
    def find_one_by_id(cls, _id):
        connection = get_db_connection()
        cursor = connection.cursor(dictionary=True)
        statement = 'SELECT entry_id, description, started_at, ended_at, user_id, created_date, project_id FROM time_entries WHERE entry_id = %s'
        args = (_id,)
        cursor.execute(statement, args)
        row = cursor.fetchone()
        if row:
            entry = TimeEntryModel(_id=row['entry_id'], user_id=row['user_id'], project_id=row['project_id'], description=row['description'], start_time = row['started_at'], end_time=row['ended_at'], created_date=row['created_date'])
            connection.close()
            return entry
        else:
            connection.close()
            return None