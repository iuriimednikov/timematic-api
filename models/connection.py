import mysql.connector as mysql

def get_db_connection():
    connection = mysql.connect(host='localhost', database='timematic', username='root', password='secret')
    return connection